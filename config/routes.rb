Leaderboard::Application.routes.draw do
  resources :teams, :submissions, :rankings
  root :to => 'rankings#index'
  match '/submissions/new', to: 'submissions#create', via: :post
end
