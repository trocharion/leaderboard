function toggle_form() {
  fs = document.getElementById('select_form');
  ps = document.getElementById('select_form_problem');
  fi = document.getElementById('input_form');
  pi = document.getElementById('input_form_problem');
  if (fs.style.display == 'none') {
    fs.style.display = 'block';
    fi.style.display = 'none';
    ps.value = pi.value;
  }
  else {
    fs.style.display = 'none';
    fi.style.display = 'block';
    pi.value = ps.value;
  }
  return false;
}
